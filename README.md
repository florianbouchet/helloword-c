# Helloword eg. in C

## Usage

### Compilation

```bash
make
```

### Run

Be sure you already compiled the source files, and then run:

```bash
./helloword 42
```

### Remove all generated files

```bash
make .PHONY
```