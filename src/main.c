#include <stdio.h>
#include <stdlib.h>
#include "parse.h"
#include "hello.h"

int main(int ac, char **av)
{
  int nb;

  if (ac != 2)
  {
    printf("%s\n", "Usage: ./helloword [nb]");
    exit(EXIT_FAILURE);
  }
  nb = parse_params(av);
  say_hello(nb);
  return EXIT_SUCCESS;
}
